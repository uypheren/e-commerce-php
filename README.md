<div align="center">
    <h1> Assignment Web Year 4</h1>
    <h3>E-Commerce website</h3>
</div>

### SQL Connection

- Directory file: `assets/connection/connect-mysql.php`

    ```
    <?php

    $dbServer = "localhost";
    $dbUsername = "root";
    $dbPassword = "1234";
    $dbName = "e-commerce";

    $conn = mysqli_connect($dbServer, $dbUsername, $dbPassword, $dbName);

    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //  echo "Connected successfully";
    ?>
    ```

### Create Database name: `e-commerce`
### Create Table

- Directory file: `assets/mysql/schema.sql`

    ```
    CREATE TABLE favorite (
        id int(11) AUTO_INCREMENT PRIMARY KEY not null,
        nr_id int(11) not null
    );

    CREATE TABLE product(
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,
    name varchar(256) not null,
    image text not null,
    price double not null,
    category text not null
    );
    ```

### Run Home Page

- Link: http://localhost:port/homepage.php

### Run Admin Page

Run admin page for add product to database.

- Link: http://localhost:port/admin/admin.php
