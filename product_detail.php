<?php
    include_once 'assets/connection/connect-mysql.php';
    include_once 'assets/php/session.php';

?>

<!Doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>New Arrival</title>

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!--linear icon css-->
		<link rel="stylesheet" href="assets/css/linearicons.css">

		<!--animate.css-->
        <link rel="stylesheet" href="assets/css/animate.css">

        <!--owl.carousel.css-->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="assets/css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <!--style.css-->
        <link rel="stylesheet" href="./assets/css/product-detail.css">
        <meta name="robots" content="noindex,follow" />
		
</head>
<body>
<!-- top-area Start -->
<div class="top-area">
    <?php include "header.php" ?>
    <div class="clearfix"></div>
</div><!-- /.top-area-->
<!-- top-area End -->

<?php 
echo "<main class='container' id='fix'>";
        echo "<!-- Image -->";
      $result = mysqli_query($conn, "SELECT id,name,FORMAT(price,2) AS 'prices',image,category FROM product where id=".$_GET['id']." ;");
      while ($row = mysqli_fetch_array($result)){
      echo "
        <div class='left-column'>
            <img data-image='red' class='active' src='./assets/images/product/".$row['image']."' width='280' height='280'>        
        </div>
        <!-- Right Column -->
        <div class='right-column'>

          <!-- Product Description -->
          <div class='product-description'>
              <span>".$row['category']."</span>
              <h1>".$row['name']."</h1>
            <p>The preferred choice of a vast range of acclaimed DJs. Punchy, bass-focused sound and high isolation. Sturdy headband and on-ear cushions suitable for live performance</p>
          </div>

          <!-- Product Configuration -->
          <div class='product-configuration'>
            <div class='single-product-detail-txt'>
              <p>
                  <i class='fa fa-star'></i>
                  <i class='fa fa-star'></i>
                  <i class='fa fa-star'></i>
                  <i class='fa fa-star'></i>
                  <span class='spacial-product-detail-icon'><i class='fa fa-star'></i></span>
                  <span class='product-detail-review'>(45 review)</span>
              </p>                                              
            </div>
          </div>

          <!-- Product Pricing -->
          <div class='product-price'>
              <span>$".$row['prices']."</span>
            <a href='{$_SERVER['PHP_SELF']}?cart=".$row['id']."&id=".$row['id']."'>
            <button type='button' class='btn btn-info'><i class='fa fa-cart-plus' aria-hidden='true'></i>  Add to cart</button>
            </a>
          </div>
        </div>
      </main>
    ";
    }
?>

<!--footer start-->
<footer class="site-footer">
	<div class="container">
        <div class="row">
          	<div class="col-sm-12 col-md-6">
            	<h6>About</h6>
            	<p class="text-justify">Shop <i> </i> is the global marketplace for unique and creative goods. It’s home to a universe of special, extraordinary items, from unique handcrafted pieces to vintage treasures. In a time of increasing automation, it’s our mission to keep human connection at the heart of commerce. That’s why we built a place where creativity lives and thrives because it’s powered by people. </p>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Categories</h6>
            	<ul class="footer-links">
				<li class="scroll"><a href="#feature">Products</a></li>
            	  <li class="scroll"><a href="newarrival.php">New Arrival</a></li>
				  <li class="scroll"><a href="#feature">Features</a></li>
            	</ul>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Our Shop</h6>
            	<ul class="footer-links">
            	  <!-- <li ><a href="">About Us</a></li> -->
            	  <li><a href="contact.php">Contact Us</a></li>
            	</ul>
          	</div>
		</div>
		<hr>
	</div>
	<div class="container">
		<div class="row">
          	<div class="col-md-8 col-sm-6 col-xs-12">
            	<p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
        		<a href="#">Shop</a>
            	</p>
          	</div>
        </div>
    </div>
	
	<div id="scroll-Top">
		<div class="return-to-top">
			<i class="fa fa-angle-up " id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div>	
	</div>

</footer>

<!-- Include all js compiled plugins (below), or include individual files as needed -->

<script src="assets/js/jquery.js"></script>

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--bootstrap.min.js-->
<script src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!--owl.carousel.js-->
<script src="assets/js/owl.carousel.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		
<!--Custom JS-->
<script src="assets/js/custom.js"></script>

</body>
</html>
