
<!doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!--linear icon css-->
		<link rel="stylesheet" href="assets/css/linearicons.css">

		<!--animate.css-->
        <link rel="stylesheet" href="assets/css/animate.css">

        <!--owl.carousel.css-->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="assets/css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="assets/css/responsive.css">

</head>
<body>
	<div class="header-area" style="background-color:#262e3c;">
		<!-- Start Navigation -->
		<div class="wrap-sticky" style="height: 114px;"><nav class="navbar navbar-default bootsnav  navbar-sticky navbar-scrollspy on menu-center no-full" data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">

			<!-- Start Top Search -->
			<div class="top-search">
						<div class="container">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-search"></i></span>
								<form action="search.php" method="GET">
									<input type="text" class="form-control" placeholder="Search" name="q">
								</form>
								<span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
							</div>
						</div>
					</div>
					<!-- End Top Search -->

					<div class="container">            
						<!-- Start Atribute Navigation -->
						<div class="attr-nav">
							<ul>
								<li class="search">
									<a href="#"><span class="lnr lnr-magnifier"></span></a>
								</li><!--/.search-->
								
								<li class="nav-favorite"><a href="favorite.php">
										<span class="lnr lnr-heart"></span>
										<span class="badge badge-bg-1" id='countf'></span>
									</a>
								</li><!--/.search-->
								
								<li class="dropdown"><a href="cart.php" class="dropdown-toggle">
										<span class="lnr lnr-cart"></span>
										<span class="badge badge-bg-1" id='countp'></span>
									</a>				                        
								</li>
							</ul>
						</div><!--/.attr-nav-->
						<!-- End Atribute Navigation -->

				<!-- Start Header Navigation -->
			<div class="navbar-header">
				                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
				                    <i class="fa fa-bars"></i>
				                </button>
				                <a class="navbar-brand"  href="homepage.php"><img src="assets/logo/brand.png" width="45" style = "width: 75px;top: 5px;" id="s"></a>

				            </div><!--/.navbar-header-->
				<!-- End Header Navigation -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu-ui-design" id="navbar-menu">
					<ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
						<li><a href="homepage.php">HOME</a></li>
						<li><a href="newarrival.php">NEW ARRIVAL</a></li>
						<li><a href="feature.php">FEATURES</a></li>
						<li><a href="contact.php">CONTACT</a></li>
					</ul><!--/.nav -->
				</div><!-- /.navbar-collapse -->
			</div><!--/.container-->
		</nav></div><!--/nav-->
		<!-- End Navigation -->
	</div>	
	<?php include 'assets/php/count.php' ?>
</body>
</html>