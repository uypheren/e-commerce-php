<?php
 	include_once 'assets/connection/connect-mysql.php';
	include_once 'assets/php/session.php';
?>

<!Doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NEW ARRIVAL</title>

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!--linear icon css-->
		<link rel="stylesheet" href="assets/css/linearicons.css">

		<!--animate.css-->
        <link rel="stylesheet" href="assets/css/animate.css">

        <!--owl.carousel.css-->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="assets/css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="assets/css/responsive.css">

		<!-- newarrival.css -->
		<link rel="stylesheet" href="assets/css/newarrival.css">

		<!-- <script src="assets/js/jquery.min.js"></script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<!-- favorite function -->
		<script type="text/javascript" src="assets/js/color.favorite.js"></script>
		
</head>
	<body onload="ChangeColor()">
		<!-- top-area Start -->
		<div class="top-area">
				<?php include "header.php" ?>
			    <div class="clearfix"></div>
			</div><!-- /.top-area-->
			<!-- top-area End -->

			<?php
                $result = mysqli_query($conn, "SELECT id,name,FORMAT(price,2) AS 'prices',image,category FROM product where category='newarrival' ;");
            echo "<section id='new-arrivals' class='new-arrivals'>";
                echo "<div class='container'>";
                    echo "<div class='section-header'>";
                        echo "<h2>NEW ARRIVAL</h2>";
                        echo "<hr>";
                        // echo "<hr>";
                    echo "</div><!--/.section-header-->";
                    echo "<div class='new-arrivals-content'>";
                        echo "<div class='row list'>";
                        
                            while ($row = mysqli_fetch_array($result)){
                                echo "<div class='col-md-3 col-sm-4 list-element'>";
                                    echo "<div class='single-new-arrival'>";
                                        echo "<div class='single-new-arrival-bg'>";
                                            //select image
                                            echo "<form method='post' enctype='multipart/form-data' action='product_detail.php'>";
                                            echo "<a href='product_detail.php?id=".$row['id']."'>";
                                                echo"<img name='image' src='../assets/images/product/".$row['image']."' >";                                                    
                                                echo "<div class='single-new-arrival-bg-overlay'></div>";
                                            echo "</a>"; 
                                            echo "</form>";
                                            echo "<div class='new-arrival-cart'>";

                                            //Add to cart
                                                echo "<p>";
                                                    echo "<a href='{$_SERVER['PHP_SELF']}?cart=".$row['id']."'>";
                                                    echo "<span class='lnr lnr-cart'></span>";
                                                    echo "add <span>to </span> cart</a>";
                                                echo "</p>";       

                                                // select favorite icon
                                                echo "<p class='arrival-review pull-right'>";                                                    
                                                echo "<a href='{$_SERVER['PHP_SELF']}?favor=".$row['id']."'>";
                                                echo "<span class='lnr lnr-heart' id='".$row['id']."'></span>";                                                       
                                                echo "<span class='lnr lnr-frame-expand'></span></a>";  
                                                echo "</p>";  
                                            echo "</div>";

                                        echo "</div>";
                                        echo "<div class='single-new-arrival-txt text-center'>";
                                            echo "<p>";
                                                echo "<i class='fa fa-star'></i>";
                                                echo "<i class='fa fa-star'></i>";
                                                echo "<i class='fa fa-star'></i>";
                                                echo "<i class='fa fa-star'></i>";
                                                echo "<span class='spacial-new-arrival-icon'><i class='fa fa-star'></i></span>";
                                                echo "<span class='new-arrival-review'>(45 review)</span>";
                                            echo "</p>";                                           
                                            echo "<h3><a href='product_detail.php?id=".$row['id']."'>".$row['name']."</a></h3>";        
                                            echo "<h5 class='new-arrival-product-price'>$".$row['prices']."</h5>";
                                        echo "</div>";
                                    echo "</div>";
                                echo "</div>";                                
                            }                            
                        echo "</div>";
                    echo "</div>";
                echo "</div><!--/.container-->";            
            echo "</section><!--/.new-arrivals-->";
            
        ?>
        <div class='section-header'>
            <button id='loadmore' class='btn-cart welcome-add-cart' >See More</button>
        </div>
        <script type="text/javascript" src="assets/js/viewmore.js"></script> 
        <!--new-arrivals end -->


<!--footer start-->
<footer class="site-footer">
	<div class="container">
        <div class="row">
          	<div class="col-sm-12 col-md-6">
            	<h6>About</h6>
            	<p class="text-justify">Shop <i> </i> is the global marketplace for unique and creative goods. It’s home to a universe of special, extraordinary items, from unique handcrafted pieces to vintage treasures. In a time of increasing automation, it’s our mission to keep human connection at the heart of commerce. That’s why we built a place where creativity lives and thrives because it’s powered by people. </p>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Categories</h6>
            	<ul class="footer-links">
				<li class="scroll"><a href="#feature">Products</a></li>
            	  <li class="scroll"><a href="newarrival.php">New Arrival</a></li>
				  <li class="scroll"><a href="#feature">Features</a></li>
            	</ul>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Our Shop</h6>
            	<ul class="footer-links">
            	  <!-- <li ><a href="">About Us</a></li> -->
            	  <li><a href="contact.php">Contact Us</a></li>
            	</ul>
          	</div>
		</div>
		<hr>
	</div>
	<div class="container">
		<div class="row">
          	<div class="col-md-8 col-sm-6 col-xs-12">
            	<p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
        		<a href="#">Shop</a>
            	</p>
          	</div>
        </div>
    </div>
	
	<div id="scroll-Top">
		<div class="return-to-top">
			<i class="fa fa-angle-up " id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div>	
	</div>

</footer>

<!-- Include all js compiled plugins (below), or include individual files as needed -->

<script src="assets/js/jquery.js"></script>

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--bootstrap.min.js-->
<script src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!--owl.carousel.js-->
<script src="assets/js/owl.carousel.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		
<!--Custom JS-->
<script src="assets/js/custom.js"></script>

</body>
</html>
