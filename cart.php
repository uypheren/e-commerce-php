<?php

include_once 'assets/connection/connect-mysql.php';
session_start();

if ( isset( $_GET['delete'] ) ) {
    unset( $_SESSION['product'][$_GET['delete']] );
}

?>

<!Doctype html>
<html class="no-js" lang="en">

    <head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>SHOPPING CART</title>
    <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <!-- <link href = '//maHeng Hongseaxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel = 'stylesheet' id = 'bootstrap-css'>
<script src = '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>

<link href = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel = 'stylesheet' integrity = 'sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN' crossorigin = 'anonymous'> -->
    <link rel='stylesheet' href='assets/css/cart.css'>
    <link rel='stylesheet' href='assets/css/carts.css'>
		
</head>
<body>

<!-- top-area Start -->
<div class="top-area">
    <?php include "header.php" ?>
    <div class="clearfix"></div>
</div><!-- /.top-area-->
<!-- top-area End -->

    <div class='container'>
        <div class='row'>
            <div class='col-12'>
                <div class='table-responsive'>
                    <table class='table table-striped'>
                        <thead>
                            <tr>
                                <th scope='col'> </th>
                                <th scope='col'>Product</th>
                                <th scope='col'>Available</th>
                                <th scope='col'>Quantity</th>
                                <th scope='col'>Price</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
$total = 0;
$count = 0;
if ( @is_array( $_SESSION['product'] ) ) {
    foreach ( $_SESSION['product'] as $id => $count ) {
        $result = mysqli_query( $conn, "SELECT id, name, image, FORMAT(price,2) AS 'prices' FROM product where id='$id' ;" );
        while ( $row = mysqli_fetch_array( $result ) ) {
            $total += $row['prices'] * $count;

            echo "
                                    <!-- Iitem -->
                                    <tr>
                                        <td><img src='assets/images/product/". $row['image'] ."' width='100' height='100'/> </td>
                                        <td>" . $row['name'] . "</td>
                                        <td>In stock</td>
                                        <td>$count</td>
                                        <td>" . $row['prices'] . " $</td>
                                        <td class='text-right'><a href='{$_SERVER['PHP_SELF']}?delete=$id'><button classtype='button' title='Delete'><i class='fa fa-trash'></i> </button></a></td>
                                    </tr>  
                                    ";

        }
    }

}
?>
                            <!-- total -->
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td class='text-right'><strong><?= "$",number_format($total,2)?> $</strong></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class='col mb-2'>
            
                <div class='row'>
                    <div class='col-sm-12 col-md-12 text-right'>
                    <a href="payment.php">
                        <button class='btn pull-right btn-lg btn-block btn-success text-uppercase '
                            id='btncheckout' style = 'width: 200px'>Checkout</button>
                            </a>
                    </div>

                </div>
             
            </div>
        </div>
    </div>

<!--footer start-->
<footer class="site-footer">
	<div class="container">
        <div class="row">
          	<div class="col-sm-12 col-md-6">
            	<h6>About</h6>
            	<p class="text-justify">Shop <i> </i> is the global marketplace for unique and creative goods. It’s home to a universe of special, extraordinary items, from unique handcrafted pieces to vintage treasures. In a time of increasing automation, it’s our mission to keep human connection at the heart of commerce. That’s why we built a place where creativity lives and thrives because it’s powered by people. </p>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Categories</h6>
            	<ul class="footer-links">
				<li class="scroll"><a href="#feature">Products</a></li>
            	  <li class="scroll"><a href="newarrival.php">New Arrival</a></li>
				  <li class="scroll"><a href="#feature">Features</a></li>
            	</ul>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Our Shop</h6>
            	<ul class="footer-links">
            	  <!-- <li ><a href="">About Us</a></li> -->
            	  <li><a href="contact.php">Contact Us</a></li>
            	</ul>
          	</div>
		</div>
		<hr>
	</div>
	<div class="container">
		<div class="row">
          	<div class="col-md-8 col-sm-6 col-xs-12">
            	<p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
        		<a href="#">Shop</a>
            	</p>
          	</div>
        </div>
    </div>
	
	<div id="scroll-Top">
		<div class="return-to-top">
			<i class="fa fa-angle-up " id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div>	
	</div>

</footer>

<!-- Include all js compiled plugins (below), or include individual files as needed -->

<script src="assets/js/jquery.js"></script>

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--bootstrap.min.js-->
<script src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!--owl.carousel.js-->
<script src="assets/js/owl.carousel.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		
<!--Custom JS-->
<script src="assets/js/custom.js"></script>

</body>
</html>
