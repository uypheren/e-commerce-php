<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery Form</title>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans"> -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/payment.css">
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png" />
    <script src="assets/js/payment.js"></script>
    <script type="text/javascript">
    window.history.forward();

    function noBack() {
        window.history.forward();
    }
    </script>
</head>

<?php
include("assets/connection/connect-mysql.php");;
error_reporting(0); // hide undefine index errors
if(isset($_POST['save']))
{	 
	 $name = $_POST['name'];
	 $phone = $_POST['phone'];
	 $email = $_POST['email'];
	 $location = $_POST['location'];
	 $getDetailProduct = "Testing";
	 $sql = "INSERT INTO delivery_details (Unn_ID, Cus_Name, Cus_Phone, Cus_Email, Cus_Location, Cus_Pro_Pus) VALUES 
	 (' ','$name','$phone','$email','$location','$getDetailProduct')" ;
	 if(mysqli_query($conn, $sql)){
		header("refresh:1;url=congratulation.php"); // redirect to index.php page
	 }
}
?>

<body>
    <div class="page-header" style="text-align: center;">
        <h1>Delivery Details Information</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto" style="left: 20%; top:30px">
                <div class="contact-form">
                    <form action="" method="post" id='form'>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputFirstName">Name <span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="name" placeholder="Your Name"
                                        required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputLastName">Phone Number <span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="phone" placeholder="Your Phone Number"
                                        required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email Address (optinal)</label>
                            <input type="email" class="form-control" name="email" placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <label for="inputMessage">Location Detial <span style="color:red">*</span></label>
                            <textarea class="form-control" name="location" rows="5" placeholder="Your Location Details"
                                required></textarea>
                        </div>
                        <input type="submit" name="save" class="btn btn-primary" value="Save and continue"
                            style="text-align: right;"></input>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>