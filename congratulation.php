<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/payment.css">
    <title>congratulation</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png" />
    <script type="text/javascript">
    window.history.forward();

    function noBack() {
        window.history.forward();
    }
    </script>
</head>

<body>
    <form method="POST">
        <div class="container">
            <div class="row" style="margin-top: 120px;">
                <div class="tab-pane" role="tabpanel" id="complete">
                    <div style="margin-top: 40px;">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </div>
                                    <h4 class="modal-title">Awesome!</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="text-center">Your booking has been confirmed. We will contact to you
                                        soon!!!.</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="homepage.php" class="btn btn-success btn-block" data-dismiss="modal"
                                        style="padding-top: 12px">
                                        Ok
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

</html>