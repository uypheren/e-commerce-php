<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="assets/css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>
    <section id="contact-block" class="contact-block">
		<div class="section-header">
			<h2>How can we help you?</h2>
        </div>
        
    	<div class="container">
        <div class="section-header"><h3 class="d-contact">We're here to help and answer any question you might have. We look forward to hearing from you.</h3></div>
        <div class="columns">
	
            <div class="column">
                
                <h1 class="cicons"><a href="#"><i class="lnr lnr-map-marker"></i></a></h1>
                <h2>Our Address</h2><h6>#2B, D.Stueng MeanChey, C.MeanChey, C.Phnom Penh, Cambodia</h6>
            </div>
            
            <div class="column">
                <h1 class="cicons"><a href="#"><i class="lnr lnr-phone-handset"></i></a></h1>
                <h2>Phone number</h2><h6>(+855) 92 354535</h6>
            </div>
        
            <div class="column">                
                <h1 class="cicons"><a href="#"><i class="lnr lnr-envelope"></i></a></h1>
                <h2>Email Address</h2><h6>Kadenshop@gmail.com</h6>
            </div>  
        </div>	
        </div>
        
        <div class="section-header">
        <button class="btn-cart welcome-add-cart" onclick="window.location.href='contact.php'"> Contact Us Now!</button>
        </div>
</section>


</body>
</html>