$(document).ready(function () {
    $("#form").validate({
        errorClass: "validationForm",
        rules: {
            name: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 11
            },
            location: {
                required: true
            },
            cardNumber: {
                required: true,
                number: true,
                minlength: 16,
                maxlength: 16
            },
            expiration: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4

            },
            cvv: {
                required: true,
                number: true,
                minlength: 3,
                maxlength: 3

            },
            cardname: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Name is required"
            },
            phone: {
                required: "Phone number is required",
                minlength: jQuery.validator.format("Phone number must be bigger than 9 digits"),
                number: jQuery.validator.format("Phone number must be number only"),
            },
            location: {
                required: "Location is required"
            },
            cardNumber: {
                required: "Card Number is required",
                maxlength: jQuery.validator.format("Card number must be 16 digits"),
                minlength: jQuery.validator.format("Card number must be 16 digits"),
                number: jQuery.validator.format("Card number must be number only"),
            },
            expiration: {
                required: "Expireation date is required",
                number: jQuery.validator.format("Expiry date must be number only"),
                minlength: jQuery.validator.format("Expiry date must bee 4 digits"),
                maxlength: jQuery.validator.format("Expiry date must bee 4 digits"),

            },
            cvv: {
                required: "Cvv is required",
                minlength: jQuery.validator.format("CVV must be 3 digits"),
                maxlength: jQuery.validator.format("CVV must be 3 digits"),
                number: jQuery.validator.format("CVV must be number only"),
            },
            cardname: {
                required: "Card owner name is required"
            }
        },


    })

});






