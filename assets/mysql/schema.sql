CREATE TABLE product(
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,
    name varchar(256) not null,
    image text not null,
    price double not null,
    category text not null
);
CREATE TABLE favorite (
    id int(11) AUTO_INCREMENT PRIMARY KEY not null,
    nr_id int(11) not null
);

CREATE TABLE cart (
    id int (11) AUTO_INCREMENT PRIMARY KEY not null,
    pro_id int (11) not null
)

-- Start pheren block sql 
-- Create payment table
CREATE TABLE `payment` (
  `cardId` varchar(10) NOT NULL,
  `cardNumber` varchar(16) NOT NULL,
  `name` varchar(20) NOT NULL,
  `expiry` varchar(4) NOT NULL,
  `cvv` varchar(3) NOT NULL,
  `balance` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Insert data into payment table 
INSERT INTO `payment` (`cardId`, `cardNumber`, `name`, `expiry`, `cvv`, `balance`) VALUES
('1111', '1111000011110000', 'UYPHEREN', '0622', '111', 50),
('2222', '0000111100001111', 'PHEREN UY', '2431', '333', 55000);

--Create delivery_details table
CREATE TABLE `delivery_details` (
  `Unn_ID` int(5) NOT NULL,
  `Cus_Name` varchar(50) NOT NULL,
  `Cus_Phone` varchar(50) NOT NULL,
  `Cus_Email` varchar(50) NOT NULL,
  `Cus_Location` varchar(200) NOT NULL,
  `Cus_Pro_Pus` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- End phern block sql
