<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Payment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <!-- For favicon png -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png" />
    <script src="assets/js/payment.js"></script>
    <script type="text/javascript">
    window.history.forward();

    function noBack() {
        window.history.forward();
    }
    </script>
</head>
<style>
.validationForm {
    color: red;
}
</style>
<?php
include("assets/connection/connect-mysql.php"); //INCLUDE CONNECTION
error_reporting(0); // hide undefine index errors
session_start();  //Start sesstion for get total amount
if ( isset( $_GET['delete'] ) ) {
    unset( $_SESSION['product'][$_GET['delete']] );
}
$getTotalAmount = 0;
$count = 0;
if ( @is_array( $_SESSION['product'] ) ) {
    foreach ( $_SESSION['product'] as $id => $count ) {
        $result = mysqli_query( $conn, "SELECT id, name, image, FORMAT(price,2) AS 'prices' FROM product where id='$id' ;" );
        while ( $row = mysqli_fetch_array( $result ) ) {
            $getTotalAmount += $row['prices'] * $count;
        }
    }
} //end total amount
if(isset($_POST['submit']))   // if button is submit
{
  $cardNumber = $_POST['cardNumber'];  //fetch records from login form
  $expiry = $_POST['expiration'];
  $cvv = $_POST['cvv'];

  $getCardBalance = null;
  $getCardBalanceSql = "SELECT balance FROM payment where cardNumber ='$cardNumber'";
  $resultBalance = $conn->query($getCardBalanceSql);
  while($row = $resultBalance->fetch_assoc()){
        $getCardBalance = $row["balance"];
    }
  $updateCardBalance = $getCardBalance - $getTotalAmount;
  
  if(!empty($_POST["submit"])) {
  $queryCardData ="SELECT *  FROM payment WHERE cvv='$cvv' && expiry = '$expiry' && cardNumber = '$cardNumber' "; //selecting matching records
  $result=mysqli_query($conn, $queryCardData); //executing
  $row=mysqli_fetch_array($result);
  if(is_array($row)) { // if matching records in the array & if everything is right   
    if($getCardBalance < $getTotalAmount){
        $balance = "Card Balance insufficient!";
    } else {
        $updateCardBalance = "UPDATE `payment` SET balance = '$updateCardBalance' WHERE cardNumber = '$cardNumber' ";
        mysqli_query($conn, $updateCardBalance);
        echo '<script>alert("Payment succesfully!, '.$getTotalAmount.'$ has debit from you card.")</script>';
        header("refresh:1;url=DeliveryDetails.php"); // redirect to index.php page
    }       
  } else {
        $message = "Card Not Found, please try again!"; 
    }
   }
}


$total = 0;
$count = 0;
if ( @is_array( $_SESSION['product'] ) ) {
    foreach ( $_SESSION['product'] as $id => $count ) {
        $result = mysqli_query( $conn, "SELECT id, name, image, FORMAT(price,2) AS 'prices' FROM product where id='$id' ;" );
        while ( $row = mysqli_fetch_array( $result ) ) {
            $total += $row['prices'] * $count;
        }
    }

}
?>

<body>
    <div class="page-header" style="text-align: center;">
        <h1>Payment Gate Ways Form</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <h3 class="text-center">Payment Details</h3>
                            <img class="img-responsive cc-img"
                                src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                        </div>
                    </div>

                    <form method="POST" id='form'>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>CARD NUMBER</label>
                                        <div class="input-group">
                                            <input type="text" name="cardNumber" class="form-control"
                                                placeholder="Valid Card Number" required />
                                            <span class="input-group-addon"><span
                                                    class="fa fa-credit-card"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-7 col-md-7">
                                    <div class="form-group">
                                        <label><span class="hidden-xs">EXPIRATION</span><span
                                                class="visible-xs-inline">EXP</span> DATE</label>
                                        <input type="text" name="expiration" class="form-control" placeholder="MM / YY"
                                            required />
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-5 pull-right">
                                    <div class="form-group">
                                        <label>CVV CODE</label>
                                        <input type="tel" name="cvv" class="form-control" placeholder="CVC" required />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>CARD OWNER</label>
                                        <input type="text" name="cardname" class="form-control"
                                            placeholder="Card Owner Names" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-12" style="text-align:center;">
                                    <h5><span style="color: red;"><?php echo $balance;?></span></h5>
                                    <h5><span style="color: red;"><?php echo $message;?></span></h5>
                                    <input type="submit" id="buttn" name="submit" class="btn btn-primary"
                                        value="Save and continue"></input>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>