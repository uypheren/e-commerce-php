
<?php
	//This is admin page for add image
  // Create database connection
	include_once '../../assets/connection/connect-mysql.php';
  // Initialize message variable
  $msg = "";

  // If upload button is clicked ...
  if (isset($_POST['upload'])) {
	// Get name
	$name = mysqli_real_escape_string($conn, $_POST['img-name']);
	// Get Price
	$price = mysqli_real_escape_string($conn, $_POST['price']);
  	// Get image name
  	$image = $_FILES['img-file']['name'];

  	// image file directory
  	$target = "../../assets/images/product/".basename($image);

  	$sql = "INSERT INTO product (name,image,price,category) VALUES ('$name', '$image', '$price','newarrival')";
  	// execute query
  	mysqli_query($conn, $sql);

  	if (move_uploaded_file($_FILES['img-file']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
  $result = mysqli_query($conn, "SELECT * FROM product where category='newarrival'");
?>
<!DOCTYPE html>
<html>
<head>
<title>Add New Arrival</title>
<style type="text/css">
   #content{
   	width: 50%;
   	margin: 20px auto;
   	border: 1px solid #cbcbcb;
   }
   form{
   	width: 50%;
   	margin: 20px auto;
   }
   form div{
   	margin-top: 5px;
   }
   #img_div{
   	width: 80%;
   	padding: 5px;
   	margin: 15px auto;
   	border: 1px solid #cbcbcb;
   }
   #img_div:after{
   	content: "";
   	display: block;
   	clear: both;
   }
   img{
   	float: left;
   	margin: 5px;
   	width: 300px;
   	height: 140px;
   }
</style>
</head>
<body>
<center>
	<h1>Add New Arrival</h1>
	<form method="get" action="../feature/index.php">
    	<button type="submit">Add Feature Product</button>
	</form>

</center>
<div id="content">
  <form method="POST" action="index.php" enctype="multipart/form-data">
  	<input type="hidden" name="size" value="1000000">
	<!-- input name -->
	<div>
		<label for="name">Name</label><br>
		<input type="text" name=img-name>
	</div>

	<!-- input price -->
	<div>
		<label for="price">Price</label><br>
		<input type="text" name=price>
	</div>

  	<!-- input image -->
	<div><input type="file" name="img-file"></div>
  	<div>
  		<button type="submit" name="upload">POST</button>
  	</div>
  </form>
	<!-- show data from database -->
	<?php
    while ($row = mysqli_fetch_array($result)) {
      echo "<div id='img_div'>";
      	echo "<img src='../../assets/images/product/".$row['image']."' >";
      	echo "<p>Name: ".$row['name']."</p>";
		echo "<p>Price: $".$row['price']."</p>";
      echo "</div>";
    }
  ?>

</div>
</body>
</html>