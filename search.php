<?php
include_once 'assets/connection/connect-mysql.php';
include_once 'assets/php/session.php';
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
	<!-- meta data -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SEARCH</title>

	<!--font-family-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

	<!-- For favicon png -->
	<link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.jpg" />

	<!--font-awesome.min.css-->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!--linear icon css-->
	<link rel="stylesheet" href="assets/css/linearicons.css">

	<!--animate.css-->
	<link rel="stylesheet" href="assets/css/animate.css">

	<!--owl.carousel.css-->
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">

	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

	<!--bootstrap.min.css-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<!-- bootsnav -->
	<link rel="stylesheet" href="assets/css/bootsnav.css">

	<!--style.css-->
	<link rel="stylesheet" href="assets/css/style.css">

	<!--responsive.css-->
	<link rel="stylesheet" href="assets/css/responsive.css">

	<!-- newarrival.css -->
	<link rel="stylesheet" href="assets/css/newarrival.css">

	<!-- <script src="assets/js/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<!-- favorite function -->
	<script type="text/javascript" src="assets/js/color.favorite.js"></script>

</head>

<body onload="ChangeColor()">
	<!-- top-area Start -->
	<div class="top-area">
		<?php include "header.php" ?>
		<div class="clearfix"></div>
	</div><!-- /.top-area-->
	<!-- top-area End -->

	<!--searched-products start -->
	<section id='new-arrivals' class='new-arrivals'>
		<div class="container">
			<div class='section-header'>
				<h2>SEARCH</h2>
				<hr>
			</div>
			<div class="new-arrivals-content">
				<div class='row list'>
					<?php
					if (isset($_GET['q'])) {
						$user_keyword = $_GET['q'];
						$con = mysqli_connect("localhost", "root", "", "e-commerce");
						$get_products = "SELECT id,name,FORMAT(price,2) AS 'prices',image FROM product WHERE keyword LIKE '%$user_keyword%'";
						$run_products = mysqli_query($con, $get_products);
						$i = 0;
						while ($row_products = mysqli_fetch_array($run_products)) {
							$i++;
							$pro_id = $row_products['id'];
							$pro_title = $row_products['name'];
							$pro_img = $row_products['image'];
							$pro_price = $row_products['prices'];

							echo "
							<div class='col-md-3 col-sm-4 list-element'>
								<div class='single-new-arrival'>
									<div class='single-new-arrival-bg'>
										<form method='post' enctype='multipart/form-data' action='product_detail.php'>
											<a href='product_detail.php?id=$pro_id'>
												<img name='image' src='./assets/images/product/$pro_img' alt='new-arrivals images'>
												<div class='single-new-arrival-bg-overlay'></div>
											</a>
										</form>
										<div class='new-arrival-cart'>
											<p>
												<a href='{$_SERVER['PHP_SELF']}?q=$user_keyword&cart=".$pro_id."'>
													<span class='lnr lnr-cart'></span>
													add <span>to </span> cart
												</a>
											</p>
											<p class='arrival-review pull-right'>
												<a href='{$_SERVER['PHP_SELF']}?q=$user_keyword&favor=$pro_id'>
												<span class='lnr lnr-heart' id='$pro_id'></span>
													<span class='lnr lnr-frame-expand'></span>
												</a>
											</p>
										</div>
									</div>
									<div class='single-new-arrival-txt text-center'>
										<p>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<span class='spacial-new-arrival-icon'><i class='fa fa-star'></i></span>
											<span class='new-arrival-review'>(45 review)</span>
										</p>
										<h3><a href='product_detail.php?id=$pro_id'>$pro_title</a></h3>
										<h5 class='new-arrival-product-price'>$$pro_price</h5>
									</div>
								</div>
							</div>
							";
						}
						if ($i == 0) {
							echo "
							<div>
								<center>
								<p>Search not found</p>
								</center>
							</div>
							";
						}
					}
					?>
				</div>
			</div>
		</div>
		<!--/.container-->
	</section>
	<!--/.searched-products-->
	<!--searched-products end-->

	<!--footer start-->
	<?php include 'footer.php'; ?>
	<!--footer end-->

	<!-- Include all js compiled plugins (below), or include individual files as needed -->

	<script src="assets/js/jquery.js"></script>

	<!--modernizr.min.js-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

	<!--bootstrap.min.js-->
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- bootsnav js -->
	<script src="assets/js/bootsnav.js"></script>

	<!--owl.carousel.js-->
	<script src="assets/js/owl.carousel.min.js"></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>


	<!--Custom JS-->
	<script src="assets/js/custom.js"></script>

</body>

</html>