<?php
include_once 'assets/connection/connect-mysql.php';
?>

<!Doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
 
        <!-- <script src="assets/js/jquery.min.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
        <!-- favorite function -->
        <script type="text/javascript" src="assets/js/color.favorite.js"></script>

        <!--product.css-->
        <link rel="stylesheet" href="assets/css/product.css">    

    </head>
	
<body onload="ChangeColor()">

<!--products start -->
<?php
	$result = mysqli_query($conn, "SELECT id,name,FORMAT(price,2) AS 'prices',image,category FROM product where category='newarrival' ;");
	echo "<section id='products' class='products'>";
		echo "<div class='container'>";
			echo "<div class='section-header'>";
				echo "<h2>KADEN SHOP</h2>";
				echo "<hr>";
			echo "</div><!--/.section-header-->";
			echo "<div class='products-content'>";
				echo "<div class='row list'>";
				
					while ($row = mysqli_fetch_array($result)){
						echo "<div class='col-md-3 col-sm-4 list-element'>";
							echo "<div class='single-product'>";
								echo "<div class='single-product-bg'>";
									//select image
									echo "<form method='post' enctype='multipart/form-data' action='product_detail.php'>";
									echo "<a href='product_detail.php?id=".$row['id']."'>";
										echo"<img name='image' src='./assets/images/product/".$row['image']."' >";                                                    
										echo "<div class='single-product-bg-overlay'></div>";
									echo "</a>"; 
									echo "</form>";
									echo "<div class='product-cart'>";

									//Add to cart
										echo "<p>";
											echo "<a href='{$_SERVER['PHP_SELF']}?cart=".$row['id']."'>";
											echo "<span class='lnr lnr-cart'></span>";
											echo "add <span>to </span> cart</a>";
										echo "</p>";       

										// select favorite icon
										echo "<p class='arrival-review pull-right'>";
											echo "<a href='{$_SERVER['PHP_SELF']}?favor=".$row['id']."'>";
											echo "<span class='lnr lnr-heart' id='".$row['id']."'></span></a>";                                                       
											echo "<span class='lnr lnr-frame-expand'></span>";  
										echo "</p>";  
									echo "</div>";

								echo "</div>";
								echo "<div class='single-product-txt text-center'>";
									echo "<p>";
										echo "<i class='fa fa-star'></i>";
										echo "<i class='fa fa-star'></i>";
										echo "<i class='fa fa-star'></i>";
										echo "<i class='fa fa-star'></i>";
										echo "<span class='spacial-product-icon'><i class='fa fa-star'></i></span>";
										echo "<span class='product-review'>(45 review)</span>";
									echo "</p>";                                           
									echo "<h3><a href='product_detail.php?id=".$row['id']."'>".$row['name']."</h3>";
									echo "<h5 class='product-product-price'>$".$row['prices']."</h5></a>";
								echo "</div>";
							echo "</div>";
						echo "</div>";                                
					}                            
				echo "</div>";
			echo "</div>";
		echo "</div><!--/.container-->";
	echo "</section><!--/.products-->";
	
?>
	<div class='section-header'>
		<button id='loadmore' class='btn-cart welcome-add-cart' > See More</button>
	</div>
<script src="assets/js/viewmore.js"></script>
</body>
</html>