<?php


require('assets/connection/connect-mysql.php');
session_start();
if(isset($_POST['submit'])){

$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];
$sql = "INSERT into contact_us (name,email,message) VALUES('$name','$email','$message')";

$success = $conn->query($sql);
echo '<script>  alert("Thanks for getting in touch.");   </script>';
include_once 'assets/php/session.php';
}
?>
<!Doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CONTACT</title>

        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <!-- For favicon png -->
		<link rel="shortcut icon" type="image/x-icon" href="assets/logo/brand.png"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!--linear icon css-->
		<link rel="stylesheet" href="assets/css/linearicons.css">

		<!--animate.css-->
        <link rel="stylesheet" href="assets/css/animate.css">

        <!--owl.carousel.css-->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- bootsnav -->
		<link rel="stylesheet" href="assets/css/bootsnav.css" >	
        
        <!--style.css-->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <!-- <script src="assets/js/jquery.min.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <!-- feature.css -->
        <link rel="stylesheet" href="assets/css/feature.css">

        <!-- favorite function -->
        <script type="text/javascript" src="assets/js/color.favorite.js"></script>
		
</head>
<body onload="ChangeColor()">

<!-- top-area Start -->
<div class="top-area">
    <?php include "header.php" ?>
    <div class="clearfix"></div>
</div><!-- /.top-area-->
<!-- top-area End -->

<div class="containers">
    <form action="contact.php" method="POST">
        <div class="form">
            <h3>CONTACT US</h3>
            <hr>
            <div class="form-group">
                <label>Name:</label>
                <input type="text" name="name" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Email:</label>
                <input type="email" name="email" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Message:</label>
                <textarea class="form-control" name="message" required></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success" name="submit" type="submit" value="submit">Submit</button>
            </div>
        </div>
    </form>
</div>

<!--footer start-->
<footer class="site-footer">
	<div class="container">
        <div class="row">
          	<div class="col-sm-12 col-md-6">
            	<h6>About</h6>
            	<p class="text-justify">Shop <i> </i> is the global marketplace for unique and creative goods. It’s home to a universe of special, extraordinary items, from unique handcrafted pieces to vintage treasures. In a time of increasing automation, it’s our mission to keep human connection at the heart of commerce. That’s why we built a place where creativity lives and thrives because it’s powered by people. </p>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Categories</h6>
            	<ul class="footer-links">
				<li class="scroll"><a href="#feature">Products</a></li>
            	  <li class="scroll"><a href="newarrival.php">New Arrival</a></li>
				  <li class="scroll"><a href="#feature">Features</a></li>
            	</ul>
          	</div>

          	<div class="col-xs-6 col-md-3">
            	<h6>Our Shop</h6>
            	<ul class="footer-links">
            	  <!-- <li ><a href="">About Us</a></li> -->
            	  <li><a href="contact.php">Contact Us</a></li>
            	</ul>
          	</div>
		</div>
		<hr>
	</div>
	<div class="container">
		<div class="row">
          	<div class="col-md-8 col-sm-6 col-xs-12">
            	<p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
        		<a href="#">Shop</a>
            	</p>
          	</div>
        </div>
    </div>
	
	<div id="scroll-Top">
		<div class="return-to-top">
			<i class="fa fa-angle-up " id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div>	
	</div>

</footer>

<!-- Include all js compiled plugins (below), or include individual files as needed -->

<script src="assets/js/jquery.js"></script>

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--bootstrap.min.js-->
<script src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!--owl.carousel.js-->
<script src="assets/js/owl.carousel.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		
<!--Custom JS-->
<script src="assets/js/custom.js"></script>

</body>
</html>
